import { ErrorMapper } from "utils/ErrorMapper";

// Spawner
import { mainSpawner } from "./Spawner";

// Roles
import { runBuilder } from "./Builder";
import { runHarvester } from "./Harvester";
import { runUpgrader } from "./Upgrader";

// When compiling TS to JS and bundling with rollup, the line numbers and file names in error messages change
// This utility uses source maps to get the line numbers and file names of the original, TS source code
export const loop = ErrorMapper.wrapLoop(() => {
	// console.log(`Current game tick is ${Game.time}`);

	// Automatically delete memory of missing creeps
	for (const name in Memory.creeps) {
		if (!(name in Game.creeps)) {
			delete Memory.creeps[name];
		}
	}

	for (const nombreSpawn in Game.spawns) {
		const spawn = Game.spawns[nombreSpawn];
		mainSpawner(spawn);
	}

	for (const nombreCreep in Game.creeps) {
		const creep = Game.creeps[nombreCreep];
		if (creep.memory.role === "harvester") {
			runHarvester(creep);
		}

		if (creep.memory.role === "upgrader") {
			runHarvester(creep);
		}

		if (creep.memory.role === "builder") {
			runHarvester(creep);
		}
	}
});

/*
const rolesPerRoom: RolesPerRoom = {
    builder:	2,
    harvester: 	5,
    upgrader:   2
}

Memory.rolesPerRoom = rolesPerRoom;
*/
