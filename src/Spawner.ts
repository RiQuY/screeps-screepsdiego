export function mainSpawner(spawn: StructureSpawn): void {
	activeCreepsRoles(spawn);

	if (spawn.energy >= 200 && Object.keys(Memory.creeps).length <= 10) {
		if (spawn.room.creepRolesInRoom.harvester < 5) {
			createHarvester(spawn);
		} else if (spawn.room.creepRolesInRoom.upgrader < 2) {
			createUpgrader(spawn);
		} else if (spawn.room.creepRolesInRoom.builder < 2) {
			createBuilder(spawn);
		}
	}
}

function createHarvester(spawn: StructureSpawn): void {
	const options = {
		memory: {
			role: "harvester",
			room: spawn.room.name,
			working: false
		}
	};
	spawn.spawnCreep([WORK, CARRY, MOVE], "Harvester " + Game.time, options);
}

function createUpgrader(spawn: StructureSpawn): void {
	const options = {
		memory: {
			role: "upgrader",
			room: spawn.room.name,
			working: false
		}
	};
	spawn.spawnCreep([WORK, CARRY, MOVE], "Upgrader " + Game.time, options);
}

function createBuilder(spawn: StructureSpawn): void {
	const options = {
		memory: {
			role: "builder",
			room: spawn.room.name,
			working: false
		}
	};
	spawn.spawnCreep([WORK, CARRY, MOVE], "Builder " + Game.time, options);
}

function activeCreepsRoles(spawn: StructureSpawn) {
	const creepRolesInRoom: CreepRolesInRoom = {
		builder: 0,
		harvester: 0,
		upgrader: 0
    };
    
	for (const nombreCreep in Memory.creeps) {
        const creep = Game.creeps[nombreCreep];
        if (creep !== undefined) {
            if (creep.memory.role === "builder") {
                creepRolesInRoom.builder++;
            } else if (creep.memory.role === "harvester") {
                creepRolesInRoom.harvester++;
            } else if (creep.memory.role === "upgrader") {
                creepRolesInRoom.upgrader++;
            }
        }
	}

	spawn.room.creepRolesInRoom = creepRolesInRoom;
}
