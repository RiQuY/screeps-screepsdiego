// example declaration file - remove these and add your own custom typings

// memory extension samples
interface CreepMemory {
	role: 	 string;
	room: 	 string;
	working: boolean;
}

interface Memory {
	uuid: 		  number;
	log:  		  any;
}

// `global` extension samples
declare namespace NodeJS {
	interface Global {
		log: any;
	}
}

interface Room {
	creepRolesInRoom: CreepRolesInRoom;
}

interface CreepRolesInRoom {
	builder: 	number;
	harvester: 	number;
	upgrader: 	number;
}
